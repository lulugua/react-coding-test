# Project Introduction
### Apply babel and webpack 4

## How to start
1. The repo is here: https://gitlab.com/lulugua/react-coding-test.git --> git clone https://gitlab.com/lulugua/react-coding-test.git
2. open project folder in terminal, run yarn install or npm i
3. run project command: npm run build:dev, then open index.js file, save it to ensure project is compiled successfully.
4. Open browser, input http://localhost:8080/
